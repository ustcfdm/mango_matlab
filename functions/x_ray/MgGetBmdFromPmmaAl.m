function bmd = MgGetBmdFromPmmaAl(L_PMMA, L_Al, energy)
% Calculate bone mineral density (BMD, g/cm2) from PMMA and Al thickness using given energy range (en).
% bmd: bone mineral density (g/cm2)
% L_PMMA: thickness of PMMA (cm)
% L_Al: thickness of Al (cm)
% energy: e.g. (20:80) (keV)

F = MgGetMaterialDecompMatrix({'PMMA', 'Al'}, {'tissue', 'hydroxyapatite'}, energy);
bmd = F(1,2)*L_PMMA + F(2,2)*L_Al;

bmd = bmd * MgGetMaterialDensity('hydroxyapatite');

end