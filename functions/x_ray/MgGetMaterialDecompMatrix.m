function F_mat = MgGetMaterialDecompMatrix(material_left, material_right, energy)
% Get the material decomposition coefficient matrix. For example:
%                 [mu_PMMA; mu_Al] = F_mat * [mu_water; mu_Ca]
% material_left: cell of material name or 1xN mu vector (e.g. {'PMMA', mu_Al})
% material_right: cell of material name or 1xN mu vector (e.g. {'water', mu_Ca})
% energy: (keV) 1xN vector

nl = numel(material_left);
nr = numel(material_right);
m = numel(energy);

% build matrix of material_left
M_left = zeros(nl, m);
for k = 1:nl
    if ischar(material_left{k}) || isstring(material_left{k})
        M_left(k,:) = MgGetLinearAttenuation(material_left{k}, energy);
    else
        M_left(k,:) = material_left{k};
    end
end

% build matrix of material_right
M_right = zeros(nr, m);
for k = 1:nr
    if ischar(material_right{k}) || isstring(material_right{k})
        M_right(k,:) = MgGetLinearAttenuation(material_right{k}, energy);
    else
        M_right(k,:) = material_right{k};
    end
end

% Find F_mat by least square fitting
F_mat = (M_left*M_right') / (M_right*M_right');

end

