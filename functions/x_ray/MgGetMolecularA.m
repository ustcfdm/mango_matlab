function A = MgGetMolecularA(formula)
% Calculate the molecular mole mass A.

[e, c] = MgParseMolecularFormula(formula);

A = 0;
for n = 1:numel(e)
    A = A + MgGetAtomicA(e{n}) * c(n);
end


end