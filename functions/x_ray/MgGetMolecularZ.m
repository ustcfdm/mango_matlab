function Z = MgGetMolecularZ(formula)
% Calculate the molecular Z.

[e, c] = MgParseMolecularFormula(formula);

Z = 0;
for n = 1:numel(e)
    Z = Z + MgGetAtomicZ(e{n}) * c(n);
end


end