function [prj_w, config, padLens] = MgSgmOffcenterWeighting(prj, config)
% Apply offcenter weighting of projection data when the detector is
% displaced along the axial direction. The projection data will also be
% padding.
% prj: 3D projection data (height x width x views).
% config: a struct including the geometry parameters. Some parameters will
% be modified after weighting.
% prj_w: 3D projection data after weighting and padding.
% padLens: the padding length (number of columns).

% Number of detector elements along u direction before padding
N = size(prj, 2);
% Detector element size along u direction
du = config.DetectorElementSize(1);
% number of views
views = size(prj, 3);

% geometry parameters
if isfield(config, 'Pmatrix')
    usePmatrix = true;
    [~, ~, sdd, uc, ~] = MgParsePmatrix(config.Pmatrix, N, size(prj,1), 0, du);
else
    usePmatrix = false;
    sdd = zeros(1, views) + config.SourceDetectorDistance;
    uc = zeros(1, views) + config.DetectorOffCenter(1);
end

% we need to judge the offcenter direction
[uc_min, idx_min] = min(uc);
[uc_max, idx_max] = max(uc);
if uc_min > 0
    direction = "right";
    uc_m = uc_max;
    idx = idx_max;
elseif uc_max < 0
    direction = "left";
    uc_m = uc_min;
    idx = idx_min;
else
    error('The offcenter direction is not consistent!');
end

% find the minimal theta
u = ((-(N-1)/2):(N-1)/2) * du + uc(idx);
gamma = atan2(u, sdd(idx));
if direction == "right"
    theta = gamma(1);
else
    theta = gamma(end);
end

%------------------------------------------------
% do the weighting view by view
%------------------------------------------------
for view = 1:views
    % u position of each detector element
    u = ((-(N-1)/2):(N-1)/2) * du + uc(view);

    % The angle (gamma) of each detector element
    gamma = atan2(u, sdd(view));

    % Calculate the weighting
    weight = -sin((pi*gamma)./(2*theta)) + 1;
    if direction == "right"
        weight(gamma>-theta) = 2;
        weight(gamma<theta) = 0;
    else
        weight(gamma>theta) = 0;
        weight(gamma<-theta) = 2;
    end

    % Apply weighting to the projection data
    prj(:,:,view) = prj(:,:,view) .* weight;
end

% Add padding to the detector shorter side
padLens = round(2* uc_m / du);
pad = zeros(size(prj,1), abs(padLens), views);

if direction == "right"
    prj_w = cat(2, pad, prj);
else
    prj_w = cat(2, prj, pad);
end

% Modify the parameters in config
if isfield(config, 'ProjectionDimension')
    config.ProjectionDimension = size(prj_w);
end
if usePmatrix
    config.PmatrixIndexShift = [max(0,padLens), 0];
    config.PmatrixCenterShift = [-max(0,padLens)*du, 0];
else
    config.DetectorOffCenter(1) = -padLens/2*du + config.DetectorOffCenter(1);
end


end

