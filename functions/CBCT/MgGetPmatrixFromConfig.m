function [pmatrix, sod, sdd, uc, vc] = MgGetPmatrixFromConfig(config, vibration)
% Calculate the p-matrix data analytically from geometrical configuration.
% vibration: (optional) a struct including the sod, sdd, uc, vc vibration range.
%            For example, vibration.sod = [1100, 1110], the sod will be
%            randomly distributed between 1100 and 1110

% detector element size
du = config.DetectorElementSize(1);
dv = config.DetectorElementSize(2);

% number of detector elements
Nu = config.ProjectionDimension(2);
Nv = config.ProjectionDimension(1);

% number of views
views = config.ProjectionDimension(3);

% source-origin distance
if nargin == 2 && isfield(vibration, 'sod')
    sod = vibration.sod(1) + (vibration.sod(2)-vibration.sod(1))*rand(views, 1);
else
    sod = repmat(config.SourceOriginDistance, [views, 1]);
end
% source-detector distance
if nargin == 2 && isfield(vibration, 'sdd')
    sdd = vibration.sdd(1) + (vibration.sdd(2)-vibration.sdd(1))*rand(views, 1);
else
    sdd = repmat(config.SourceDetectorDistance, [views, 1]);
end

% detector offcenter (uc)
if nargin == 2 && isfield(vibration, 'uc')
    uc = vibration.uc(1) + (vibration.uc(2)-vibration.uc(1))*rand(views, 1);
else
    uc = repmat(config.DetectorOffCenter(1), [views, 1]);
end
% detector offcenter (vc)
if nargin == 2 && isfield(vibration, 'vc')
    vc = vibration.vc(1) + (vibration.vc(2)-vibration.vc(1))*rand(views, 1);
else
    vc = repmat(config.DetectorOffCenter(2), [views, 1]);
end

% the position of upper left detector element, index is [1, 1] (Matlab style) or [0, 0] (C++ style)
u0 = -(Nu-1)/2*du + uc;
v0 = -(Nv-1)/2*dv + vc;

% scan angle of each view
d_beta = config.ScanAngle / views;  % [degree]
beta = ((0:views-1) * d_beta + config.ImageRotation)* pi / 180; % [radius]



% calculate the p-matrix
pmatrix = zeros(12, views);

% if (config.FlipDetectorDirection && config.ScanAngle>0) || (~config.FlipDetectorDirection && config.ScanAngle<0)
for v = 1:views
    if config.FlipDetectorDirection
        p = [    -sin(beta(v)),      cos(beta(v)), 0,       0;
            0,                 0, 1,       0;
            -cos(beta(v))/sdd(v), -sin(beta(v))/sdd(v), 0, sod(v)/sdd(v)];
    else
        p = [     sin(beta(v)),     -cos(beta(v)), 0,       0;
            0,                 0, 1,       0;
            -cos(beta(v))/sdd(v), -sin(beta(v))/sdd(v), 0, sod(v)/sdd(v)];
    end

    % the physics-to-index conversion matrix
    C = [1/du, 0, -u0(v)/du;
        0, 1/dv, -v0(v)/dv;
        0,    0,      1];

    pm = (C * p)';
    pmatrix(:,v) = pm(:);

end
% if config.FlipDetectorDirection
%     for v = 1:views
%         p = [    -sin(beta(v)),      cos(beta(v)), 0,       0;
%             0,                 0, 1,       0;
%             -cos(beta(v))/sdd, -sin(beta(v))/sdd, 0, sod/sdd];
% 
%         pm = (C * p)';
%         pmatrix(:,v) = pm(:);
%     end
% else
%     for v = 1:views
%         p = [     sin(beta(v)),     -cos(beta(v)), 0,       0;
%             0,                 0, 1,       0;
%             -cos(beta(v))/sdd, -sin(beta(v))/sdd, 0, sod/sdd];
% 
%         pm = (C * p)';
%         pmatrix(:,v) = pm(:);
%     end
% end

end