function [beta, sod, sdd, uc, vc] = MgParsePmatrix(pmatrix, Nu, Nv, indexStartNumber, du)
% Parse p-matrix data and calculate the scanning geometry.
% beta: scan angle [degree]
% sod: source-origin distance [mm]
% sdd: source-detector distance [mm]
% uc: detector offcenter w.r.t central ray along u direction [mm]
% vc: detector offcenter w.r.t central ray along v direction [mm]
% pmatrix: p-matrix data (12 rows)
% Nu, Nv: number of detector elements along u, v direction, respectively
% indexStartNumber: 0 (default) (C/C++ style) or 1 (Matlab style)
% du: (optional) detector element size [mm]

[rows, views] = size(pmatrix);
if rows ~= 12
    error("The pmatrix data should have 12 rows!\n");
end

if nargin < 4
    indexStartNumber = 0;
end


% malloc space for output data
sod = zeros(views, 1);
sdd = zeros(views, 1);
beta = zeros(views, 1);
uc = zeros(views, 1);
vc = zeros(views, 1);

for view = 1:views
    %==================================================
    % Step 1: inverse info of p-matrix
    %==================================================
    p = reshape(pmatrix(:,view), 4, 3)';
    A = inv(p(:,1:3));

    if nargin == 5
        lambda = du / norm(A(:,1));
        p = p / lambda;
        A = inv(p(:,1:3));
    end

    e_u = A(:,1);
    e_v = A(:,2);

    r_s = -A * p(:,4);
    r_d = r_s + A(:,3);

    %==================================================
    % Step 2: calculate geometry parameters
    %==================================================
    % scan angle
    beta(view) = atan2d(r_s(2), r_s(1));

    % source-origin distance
    sod(view) = norm(r_s, 2);

    % source-detector distance
    n_v = cross(e_u, e_v);  % normal vector of e_u-e_v plane
    r_sd = r_d - r_s;       % vector from source to detector
    sdd(view) = abs(dot(r_sd, n_v)) / norm(n_v, 2);

    % offcenter
    ui_c = (Nu-1)/2 + indexStartNumber; % index of detector center
    vi_c = (Nv-1)/2 + indexStartNumber; % index of detector center
    alpha = dot(r_d, n_v) / dot(r_s, n_v);
    r_oc = r_d + ui_c*e_u + vi_c*e_v - alpha*r_s;   % vector of offcenter
    uc(view) = dot(r_oc, e_u) / norm(e_u, 2);
    vc(view) = dot(r_oc, e_v) / norm(e_v, 2);

end

% smooth scan angle
for view = 2:views
    if beta(view) - beta(view-1) > 90
        beta(view) = beta(view) - 360;
    elseif beta(view) - beta(view-1) < -90
        beta(view) = beta(view) + 360;
    end
end


end