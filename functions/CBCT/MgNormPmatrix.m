function pmatrix = MgNormPmatrix(pmatrix, du)
% Normalize the p-matrix based on the given detector pixel size du.
% pmatrix: 12 rows p-matrix data.
% du: detector pixel size.

[rows, views] = size(pmatrix);
if rows ~= 12
    error("The pmatrix data should have 12 rows!\n");
end



for view = 1:views
    p = reshape(pmatrix(:,view), 4, 3)';
    A = inv(p(:,1:3));

    lambda = du / norm(A(:,1));
    pmatrix(:,view) = pmatrix(:,view) / lambda;

end


end

