function img = MgCbctNmar(prj, config, metal_threshold, uniform_range)
% Perform Normalized Metal Artifact Reduction (NMAR) for prj.
% prj: projection data
% config: config for fbp and fpj
% metal_threshold: the threshold of metal region
% uniform_range: the soft tissue range [a, b]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 1: MAR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
small_number = 1e-7;
config.ProjectionDimension = size(prj);
%=========================================================================
% Step 1: Reconstruct images and get the ROI of metal
%=========================================================================
% Use cos reconstruction kernel
cfg = config;
cfg.KernelName = 'Hamming';
cfg.KernelParas = 0;

img1 = mgfdk(prj, cfg);

% the metal ROI
img_metal = img1 > metal_threshold;

%=========================================================================
% Step 2: Forward project metal ROI, do the interpolation, and reconstruct
%         images with MAR
%=========================================================================
prj_metal = mgfpj(single(img_metal), config);

% perform interpolation row by and row, and view by view
idx_prj_metal = prj_metal>small_number;
prj_mar = MgInterpSinogramMetal1D(prj, idx_prj_metal);

% reconstruct images with MAR
img_mar = mgfdk(prj_mar, config);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 2: NMAR (to be completed)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%=========================================================================
% Step 3: generate prior image and normalization
%=========================================================================
img_prior = img_mar;
idx_uniform = img_mar >= uniform_range(1) & img_mar <= uniform_range(2);
img_prior(idx_uniform) = mean(img_mar(idx_uniform), 'all');

prj_prior = mgfpj(img_prior, cfg);

prj_nmar = MgInterpSinogramMetal1D(prj, idx_prj_metal, prj_prior);
% smooth the interpolated projection data
prj_nmar_smooth = imgaussfilt(prj_nmar, 1);

prj(idx_prj_metal) = prj_nmar_smooth(idx_prj_metal);

img = mgfdk(prj, config);

img(img_metal) = img1(img_metal);



end