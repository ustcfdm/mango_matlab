function pmatrix = MgGetPmatrixBigSmallBBPhantom(Image, AreaSV, upsideDown)
% Get the p-matrix data from the calibration phantom with big and small BBs.
% Image: projection 3D image (pre-log).

% Image size
[ImageRows, ImageColumns, ImageNum] = size(Image);

if nargin < 3
    upsideDown = false;
end

Display = 477;

Detector = '3030';
FOV = 30;

%----------------------SoftTissueCT Protocol-------------------------------
%
%                        30x30        30x40
%
% Spin DR               978*978      952*1294
% Spin DR lighting      652*652      474*648
% GCT                   978*978      952*1294
% GCT - HDR            1024*1024    1032*1032

% SV : StandardValue
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch Detector
    
    case '3030'
        
        if FOV == 30

            RowSV = 1956;
            ColumSV = 1956;

            DiskRadiusSV = 400; 
%             AreaSV = 416;
%             AreaSV = 600;   % modified by Mango

            LeftLimitSV = 560;
            RightLimitSV = 1400;
            UpLimitSV = 320;
            DownLimitSV = 1800; 
            HeightLimitSV = 36;   
            WidthLimitSV = 60;

        elseif FOV == 16

            RowSV = 1024;
            ColumSV = 1024;

            DiskRadiusSV = 400; 
            AreaSV = 416;

            LeftLimitSV = 100;
            RightLimitSV = 950;
            UpLimitSV = 50;
            DownLimitSV = 950; 
            HeightLimitSV = 36;   
            WidthLimitSV = 60;
        end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    case '3040'
     
        if FOV == 30

            RowSV = 1904;
            ColumSV = 2586;

            DiskRadiusSV = 400; 
            AreaSV = 400;

            LeftLimitSV = 800;
            RightLimitSV = 1760;
            UpLimitSV = 320;
            DownLimitSV = 1800; 
            HeightLimitSV = 36;   
            WidthLimitSV = 60;

        elseif FOV == 16

            RowSV = 1032;
            ColumSV = 1032;

            DiskRadiusSV = 400; 
            AreaSV = 416;

            LeftLimitSV = 100;
            RightLimitSV = 950;
            UpLimitSV = 50;
            DownLimitSV = 950; 
            HeightLimitSV = 36;   
            WidthLimitSV = 60;
        end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BulletAngleIncrement = 2 * pi/360 * 9;   % 小球角度增量
BulletHeightIncrement = 1.3;        % 体模高度增量 mm
PhantomRadius = 65;         % 体模半径 mm
BulletTotalNumber = 108;

RowsRatio = RowSV/ImageRows;
ColumsRatio = ColumSV/ImageColumns;

DiskRadius = ceil(sqrt(DiskRadiusSV/(RowsRatio * ColumsRatio)));
BulletAreaThreshold = AreaSV/(RowsRatio * ColumsRatio);

LeftLimit = ceil(LeftLimitSV/ColumsRatio);
RightLimit = floor(RightLimitSV/ColumsRatio);
UpLimit = ceil(UpLimitSV/RowsRatio);
DownLimit = floor(DownLimitSV/RowsRatio); 
HeightLimit = HeightLimitSV/ColumsRatio;   
WidthLimit = WidthLimitSV/RowsRatio;

%%  Bullet Coordinate Calculation
for i = 1 : BulletTotalNumber 
    theta = 90/180 * pi - (i-61) * BulletAngleIncrement;
    BulletCoord(i,:) = [PhantomRadius * sin(theta), ...
              PhantomRadius * cos(theta), ...
              -8-(i-61) * BulletHeightIncrement];  
end

%% The BBs order
OriginalOrder = [1 1 1 1 1 1 1 0 1 1 1 0 1 0 0 1 1 1 1 0 1 0 1 0 1 0 0 1 0 0 1 1 0 1 0 0 0 0 1 1 0 1 1 0 1 0 1 1 1 0 0 1 0 0 0 1 1 1 0 1 1 0 0 1 1 1 0 0 0 0 0 1 0 1 0 1 1 0 0 0 1 0 1 1 1 1 1 0 0 0 1 1 0 0 1 0 1 0 0 0 1 0 0 0 0 0 0 0 ]';
if upsideDown
    OriginalOrder = flip(OriginalOrder);
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the following line is modified by Mango
for n = 1 : ImageNum
    OriginalImage = Image(:,:,n);

     for i = 1 : ImageColumns
            FlipImage(:,i ) = OriginalImage(:,i);
    end

    if n == Display
        figure(1);imshow(FlipImage,[]);title('Original Projection');
    end
    
    %% bottom-hat transform
    Se = strel('disk',DiskRadius);   % 创建圆盘半径
    BhImage = imbothat(FlipImage,Se);
    if n == Display
        figure(2);imshow(BhImage,[]);title('bottom hat');
    end

    %% Binary image
    
    BhImage = im2double(BhImage);
    level = 0.001;                      
    BhImageMax = max(BhImage(:));                
    BhImageMin = min(BhImage(:));                  
    T1 = (BhImageMax+BhImageMin) / 2;              
    ooo = 0;
    ANum = 0;                         
    BNum = 0;                             
    AAll = 0;                             
    BAll = 0;                              
    for i = 1 : ImageRows
        for j = 1 : ImageColumns
            if (BhImage(i,j) >= T1)
                ANum = ANum + 1;                
                AAll = AAll + BhImage(i,j);    
            elseif (BhImage(i,j)<  T1)
                BNum = BNum + 1;              
                BAll = BAll + BhImage(i,j);     
            end
        end
    end
    AAve = AAll / ANum;                     
    BAve = BAll / BNum;                     
    T2 = (AAve + BAve) / 2;                  
    while (abs(T2 - T1) > level)              
        T1 = T2;                       
        ANum = 0;
        BNum = 0;
        AAll = 0;
        BAll = 0;
        for i = 1 : ImageRows
            for j = 1 : ImageColumns
                if (BhImage(i,j)>= T1)
                    ANum = ANum + 1;
                    AAll = AAll + BhImage(i,j);
                elseif (BhImage(i,j) < T1)
                    BNum = BNum + 1;
                    BAll = BAll + BhImage(i,j);
                end
            end
        end
        AAve = AAll / ANum;
        BAve = BAll / BNum;  
        T2 = (AAve + BAve) / 2; 
        ooo = ooo + 1;
    end                                  
    oo(n) = ooo;
    BW = BhImage;                     
    for i = 1 : ImageRows
        for j = 1 : ImageColumns
            if BhImage(i,j) >= T2
                BW(i,j) = 1;
            else
                BW(i,j) = 0;
            end
        end
    end
    TH(n) = T2;
    if n == Display
        figure(3);imshow(BW); axis on; title('Binary');
    end
%%  Bullet Status Calculation
    %% Region Connection
    LabelMap = zeros(size(BW));     %标记图像
    LabelIndex = 1;
    QueueHead = 1;       %队列头
    QueueTail = 1;       %队列尾
    SumX = 0;
    SumY = 0;
    Area = 0;
    Neighbour = [-1 -1;-1 0;-1 1;0 -1;0 1;1 -1;1 0;1 1];  %和当前像素坐标相加得到八个邻域坐标

    for i = UpLimit : DownLimit
        for j = LeftLimit : RightLimit
            if BW(i,j) == 1 && LabelMap(i,j) == 0           
                LabelMap(i,j) = LabelIndex;
                Queue{QueueTail} = [i j];        %用元组模拟队列，当前坐标入列
                QueueTail = QueueTail + 1;
                while QueueHead ~= QueueTail
                    pix = Queue{QueueHead};
                    for k = 1:8               %8邻域搜索
                        pix1 = pix + Neighbour(k,:);
                        if pix1(1) >= 2 && pix1(1) <= ImageRows - 1 && pix1(2) >= 2 &&pix1(2) <= ImageColumns - 1
                            if BW(pix1(1),pix1(2)) == 1 && LabelMap(pix1(1),pix1(2)) == 0  %如果当前像素邻域像素为1并且标记图像的这个邻域像素没有被标记，那么标记
                                LabelMap(pix1(1),pix1(2)) = LabelIndex;
                                Queue{QueueTail} = [pix1(1) pix1(2)];
                                QueueTail = QueueTail + 1;
                            end
                        end
                    end
                    QueueHead = QueueHead + 1;
                end
                clear Queue;                %清空队列，为新的标记做准备
                LabelIndex = LabelIndex + 1;
                QueueHead = 1;
                QueueTail = 1;
            end
        end
    end
    LabelIndex = LabelIndex - 1;
    
    %% Centroid and Area Calculation
    p = 0;
    for k = 1 : LabelIndex
        for i = 1 : ImageRows
            for j = 1 : ImageColumns
                if LabelMap(i,j) == k
                    SumX = SumX + i;
                    SumY = SumY + j;
                    Area = Area + 1;
                end
            end
        end
        p = p + 1;
        BulletStatus(p,2) = fix(SumX/Area) - 1;
        BulletStatus(p,1) = fix(SumY/Area) - 1;
        % Classification
        if Area > BulletAreaThreshold    %%%%%%%%%%%%%%%%%%%%
           BulletStatus(p,3) = 1;     % big bullets
        else
           BulletStatus(p,3) = 0;     % small bullets
        end
        AreaTemp(p) = Area;
        SumX = 0;
        SumY = 0;
        Area = 0;
    end
    [BulletStatus(:,2),BulletIndexY] = sort(BulletStatus(:,2),1);   %按y坐标升序排列 
    BulletStatus(:,1) = BulletStatus(BulletIndexY,1);
    BulletStatus(:,3) = BulletStatus(BulletIndexY,3);
    AreaTemp = AreaTemp(BulletIndexY);
    BulletStatus(:,5) = AreaTemp;
    
    
%% Match Bullet Order
    %% Seperate bullets to different parts，每7个点是不同的，可以大于等于7个，如果小于7个可能会出问题
     GroupRowNum = 1;
     GroupColumnNum = 1;
     BulletGroup(GroupRowNum,GroupColumnNum) = 1;
     for k = 2 : length(BulletStatus)
         if BulletStatus(k,2) - BulletStatus(k - 1,2) < HeightLimit && abs(BulletStatus(k,1) - BulletStatus(k - 1,1)) > WidthLimit %%%%%%%%%%%%%%%%%%%% 
             GroupColumnNum = GroupColumnNum + 1;
             BulletGroup(GroupRowNum,GroupColumnNum) = k;
         else
             BulletCount(GroupRowNum) = GroupColumnNum;
             GroupColumnNum = 1;
             GroupRowNum = GroupRowNum + 1;
             BulletGroup(GroupRowNum,GroupColumnNum) = k;
         end
         BulletCount(GroupRowNum) = GroupColumnNum;
     end
    
    for j = 1 : GroupRowNum
        ZeroLength(j) = length(find(BulletGroup(j,:) == 0));
        NonzeroLength(j) = length(BulletGroup) - ZeroLength(j);
    end
    
    DiscardIndex1 = find(NonzeroLength < 7);
    BulletGroup(DiscardIndex1,:) = [];
    BulletCount(DiscardIndex1) = [];
    GroupRowNum = size(BulletGroup,1);
    
    if GroupRowNum < 2
        errordlg('计算失败！请检查体模摆放是否倒置或者超出范围，返回重新开始。');
        return
    end
    
    GroupTest(n) = GroupRowNum; 
     
    if n == Display
       figure(4),imagesc(FlipImage);%将矩阵I中的元素数值按大小转化为不同颜色
       colormap gray;% 返回线性灰度色图
       axis image;
       hold on;
       for ii = 1 : GroupRowNum
         plot(BulletStatus(BulletGroup(ii,1:BulletCount(ii)),1),BulletStatus(BulletGroup(ii,1:BulletCount(ii)),2),'.'),hold on;
       end
       hold off;
    end

    kk = 1;
    for ii = 1:GroupRowNum
        BulletGroupTemp = BulletGroup(ii,1:BulletCount(ii));
        while sum(BulletStatus(BulletGroupTemp,3) == OriginalOrder(kk:kk + BulletCount(ii) - 1)) ~= BulletCount(ii) 
            kk = kk + 1;           
        end
        BulletStatus(BulletGroupTemp,4) = [kk:kk + BulletCount(ii) - 1]';     
        kk = kk + BulletCount(ii);
        clear BulletGroupTemp
    end
    DiscardIndex2 = find(BulletStatus(:,4) == 0);
    BulletStatus(DiscardIndex2,:) = [];
%     if Saveall
%         xlswrite([num2str(Name),'BulletStatus.xlsx'],BulletStatus,num2str(n));
%     end
    BulletNum = length(BulletStatus);
    % Match Bullet Coordinates
    q = BulletCoord(BulletStatus(:,4),:); 
    
%% Projection Matrix Calculation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    A = [];
    for i = 1 : BulletNum
        temp = [q(i,1),q(i,2),q(i,3),1,0,0,0,0,-q(i,1)*BulletStatus(i,1),-q(i,2)*BulletStatus(i,1),-q(i,3)*BulletStatus(i,1),-BulletStatus(i,1)
                0,0,0,0,q(i,1),q(i,2),q(i,3),1,-q(i,1)*BulletStatus(i,2),-q(i,2)*BulletStatus(i,2),-q(i,3)*BulletStatus(i,2),-BulletStatus(i,2)];
        A = [A;temp];
    end
    [~,D1,V] = svd(A);
    D2 = svd(A);
    PM(:,n) = V(:,size(D2,1)); 
    PM(:,n) = PM(:,n)/PM(12,n);

    disp([num2str(n),' Projection Matrix has been completed ...'])
    clear BulletStatus BulletGroup BulletCount ZeroLength NonzeroLength DiscardIndex1 DiscardIndex2;
end

pmatrix = PM;



end