function c = MgColorMapRedWhiteBlue(m)
%c =  MgColorMapRedWhiteBlue(m)
%   MgColorMapRedWhiteBlue(m) returns an M-by-3 matrix containing a colormap with specified rgb color.
%   MgColorMapRedWhiteBlue, by itself, is the same length as the current figure's
%   colormap. If no figure exists, MATLAB uses the length of the
%   default colormap.
%
%   For example, to reset the colormap of the current figure:
%
%       colormap(MgColorMapRedWhiteBlue)
%


if nargin < 1
   f = get(groot,'CurrentFigure');
   if isempty(f)
      m = size(get(groot,'DefaultFigureColormap'),1);
   else
      m = size(f.Colormap,1);
   end
end


n1 = round(m/2);
n2 = m - n1 + 1;


r1 = linspace(0,1,n1)';
g1 = r1;
b1 = ones(n1, 1);

r2 = ones(n2, 1);
g2 = linspace(1,0,n2)';
b2 = g2;

c1 = cat(2, r1, g1, b1);
c2 = cat(2, r2, g2, b2);

c = cat(1, c1, c2(2:end,:));

end
