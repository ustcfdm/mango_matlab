function img = MgCropImgBorder(img, border_color)
% Remove image marginal border.
% img: 3D image data.
% border_color: an array with three elements [R, G, B] (optional, default is image upper left color).

if nargin < 2
    border_color = img(1,1,1:3);
end

border_color = reshape(border_color, [1, 1, 3]);

% compare color
img_comp = img(:,:,1:3) == border_color;
img_comp = img_comp(:,:,1) & img_comp(:,:,2) & img_comp(:,:,3);

% number of rows and cols
[rows, cols] = size(img_comp);

% sum over rows
col_profile = sum(img_comp, 1);
% sum over cols
row_profile = sum(img_comp, 2);

% detect 
idx_col = find(col_profile < rows);
idx_row = find(row_profile < cols);

% left, right index
left = min(idx_col);
right = max(idx_col);
% up, down index
up = min(idx_row);
down = max(idx_row);

% do crop
img = img(up:down, left:right, :);

end

