function MgBarPlotWithErrorBars(data_series, data_error, baseValue, colorTheme)
% Plot multiple bars with error bars.

if nargin < 4
    colorTheme = 'light';
end
if nargin < 3
    baseValue = 0;
end

b1 = bar(data_series, 'BaseValue', baseValue);

hold on;

% Calculate the number of groups and number of bars in each group
[ngroups,nbars] = size(data_series);

% Get the x coordinate of the bars
x = nan(nbars, ngroups);
for i = 1:nbars
    x(i,:) = b1(i).XEndPoints;
end

% Plot the errorbars
b2 = errorbar(x',data_series,data_error, 'k', 'linestyle','none');

hold off


if colorTheme == "dark"
    for n = 1:numel(b1)
        b1(n).EdgeColor = 'w';
    end
    for n = 1:numel(b2)
        b2(n).Color = 'w';
    end
end


end

