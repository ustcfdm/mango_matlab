function [obj_folder_pmma, obj_folder_Al] = MgBenchtopDualEnergyDecompPrelog(config_filename, badPixelCorr)
% Perform projection domain material decomposition and generate sinogram for benchtop PCD-CT system.

js = MgReadJsoncFile(config_filename);

if nargin < 2
    badPixelCorr = false;
end

%% folders to save decomp sinogram and img (w/ and w/o rebin)
obj_folder_pmma = 'DE/prelog_PMMA_decomp';
obj_folder_Al = 'DE/prelog_Al_decomp';

folder_sgm_PMMA = sprintf('./sgm/%s/%s', js.ObjectName, obj_folder_pmma);
folder_sgm_Al = sprintf('./sgm/%s/%s', js.ObjectName, obj_folder_Al);

MgMkdir(folder_sgm_PMMA, false);
MgMkdir(folder_sgm_Al, false);

%% load panel correction parameters
load(sprintf('%s/cali_DE.mat', MgBenchtopGetPanelCorrProtocolFolder(js.PanelCorrProtocol)));
cali_PMMA = cali_PMMA{2, 2};
cali_Al = cali_Al{2, 2};
% reslice calibration parameters
cali_PMMA = MgResliceCaliPcell(cali_PMMA, js.SliceStartIdx, js.SliceThickness, js.SliceCount);
cali_Al = MgResliceCaliPcell(cali_Al, js.SliceStartIdx, js.SliceThickness, js.SliceCount);

%=========================================================
% read files and do decomposition
%=========================================================
% read EVI air data
file_air_LE = sprintf('./EVI/%s/air_LE.EVI', js.AirName);
file_air_HE = sprintf('./EVI/%s/air_HE.EVI', js.AirName);
if ~isfile(file_air_LE)
    file_air_TE = sprintf('./EVI/%s/air_TE.EVI', js.AirName);
    MgConvertHeTeToLe(file_air_TE, file_air_HE, file_air_LE);
end
prj_air_LE = mean(MgReadEviData(file_air_LE), 3);
prj_air_HE = mean(MgReadEviData(file_air_HE), 3);
prj_air_LE = MgResliceProjectionToSinogram(prj_air_LE, js.SliceStartIdx, js.SliceThickness, js.SliceCount);
prj_air_HE = MgResliceProjectionToSinogram(prj_air_HE, js.SliceStartIdx, js.SliceThickness, js.SliceCount);


% axial width after rebin
widthRebin = size(prj_air_HE,2) / js.RebinSize;

% obj file names
[files_TE_short, files_TE_long] = MgDirRegExpV2(sprintf('./EVI/%s', js.ObjectName), sprintf('%s_TE.EVI', js.ObjectIndex));
[files_HE_short, files_HE_long] = MgDirRegExpV2(sprintf('./EVI/%s', js.ObjectName), sprintf('%s_HE.EVI', js.ObjectIndex));

for n = 1:numel(files_HE_short)
    %-------------------------------------------
    % Step 1: read EVI and take log
    %-------------------------------------------
    file_LE = strrep(files_HE_long{n}, '_HE.EVI', '_LE.EVI');
    if ~isfile(file_LE)
        MgConvertHeTeToLe(files_TE_long{n}, files_HE_long{n}, file_LE);
    end
    prj_LE = MgReadEviData(file_LE);
    prj_HE = MgReadEviData(files_HE_long{n});
    
    % apply bad pixel correction
    if badPixelCorr
        prj_LE = MgBenchtopBadPixelCorr(prj_LE);
        prj_HE = MgBenchtopBadPixelCorr(prj_HE);
    end
    
    % pre-log bin
    prj_LE = MgResliceProjectionToSinogram(prj_LE(:,:,1:js.Views), js.SliceStartIdx, js.SliceThickness, js.SliceCount);
    prj_HE = MgResliceProjectionToSinogram(prj_HE(:,:,1:js.Views), js.SliceStartIdx, js.SliceThickness, js.SliceCount);
    
    % take log
    prj_log_LE = log(prj_air_LE ./ prj_LE);
    prj_log_LE(isnan(prj_log_LE)) = 0;
    prj_log_LE(isinf(prj_log_LE)) = 0;
    prj_log_HE = log(prj_air_HE ./ prj_HE);
    prj_log_HE(isnan(prj_log_HE)) = 0;
    prj_log_HE(isinf(prj_log_HE)) = 0;
    
    % save memory
    clear prj_LE prj_HE

    %-------------------------------------------
    % Step 2: do the DE decomposition
    %-------------------------------------------
    sgm_PMMA = MgPolyvalTwoVariable(cali_PMMA, prj_log_LE, prj_log_HE, false);
    sgm_Al = MgPolyvalTwoVariable(cali_Al, prj_log_LE, prj_log_HE, false);
    
    % save memory
    clear prj_log_LE prj_log_HE
    
    sgm_PMMA(isnan(sgm_PMMA)) = 0;
    sgm_PMMA(isinf(sgm_PMMA)) = 0;
    sgm_Al(isnan(sgm_Al)) = 0;
    sgm_Al(isinf(sgm_Al)) = 0;

    %-------------------------------------------
    % Step 3: interp white lines
    %-------------------------------------------
    sgm_PMMA = MgInterpSinogram(sgm_PMMA, 1, 2, 4);
    sgm_Al = MgInterpSinogram(sgm_Al, 1, 2, 4);
    
    %-------------------------------------------
    % Step 4: rebin sinogram
    %-------------------------------------------
    sgm_PMMA_rebin = MgRebinSinogram(sgm_PMMA, js.RebinSize);
    sgm_Al_rebin = MgRebinSinogram(sgm_Al, js.RebinSize);
    
    % save to file
    filename = strrep(files_HE_short{n}, '_HE.EVI', sprintf('_%d-%d-%d.raw', widthRebin, js.Views, js.SliceCount));
    MgSaveRawFile(sprintf('%s/sgm_%s', folder_sgm_PMMA, filename), sgm_PMMA_rebin);
    MgSaveRawFile(sprintf('%s/sgm_%s', folder_sgm_Al, filename), sgm_Al_rebin);
end

end

