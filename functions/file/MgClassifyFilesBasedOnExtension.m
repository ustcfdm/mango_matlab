function MgClassifyFilesBasedOnExtension(inFolder, outFolder)
% In the given folder (inFolder), there may be several files with different file
% extensions (e.g. dat, png, raw). This function with move them into
% corresponding folders based on their extensions.
% inFolder: The folder that contains multiple files.
% outFolder: (optional) The folder that you want to classify the files
% (default is '.');

% create outFolder
if nargin < 2
    outFolder = '.';
end
MgMkdir(outFolder, false);

% get the files
[fs, fl] = MgDirRegExpV2(inFolder, '.*');

% get the file extension category
[~, ~, ext] = fileparts(fs);
ext = unique(ext);

% create output subfolders
fd_out = cell(1, numel(ext));
for n = 1:numel(ext)
    fd_out{n} = fullfile(outFolder, ext{n}(2:end));
    MgMkdir(fd_out{n}, false);
end

% move the files
for n = 1:numel(fl)
    [~,~,ext] = fileparts(fs{n});
    % destinate file name
    filename = fullfile(outFolder, ext(2:end), fs{n});
    
    [~, a] = fileparts(fileparts(fl{n}));
    [~, b] = fileparts(fileparts(filename));
    if string(a) ~= string(b)
        movefile(fl{n}, filename);
    end
end


end