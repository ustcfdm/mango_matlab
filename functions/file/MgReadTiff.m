function data = MgReadTiff(filename, dType)
% data = MgReadRawFile(filename)
% This function reads tiff file as a volume. Arguments:
% filename: the name of the file
% dType: data type of data (default is 'double')

warning('off', 'imageio:tiffmexutils:libtiffWarning');

if nargin < 2
    dType = 'double';
end

t = Tiff(filename, 'r');
info = imfinfo(filename);

w = info(1).Width;
h = info(1).Height;
s = length(info);

data = zeros(h, w, s, dType);

for k = 1:s
    t.setDirectory(k);
    data(:,:,k) = cast(t.read(), dType);
end

t.close();

warning('on', 'imageio:tiffmexutils:libtiffWarning');

end

