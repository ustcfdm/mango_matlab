function [img, pics, pos] = MgReadMammoRawFile(filename, addChipBorder)
% Read mammo raw file, return the image of each chip and merged image.
% img: 2D image of each chip tiled side by side.
% pics: 3D volume, each slice is the image of each chip (288 × 288).
% pos: 72 by 2 matrix, each row is the position of each chip upper left corner.
% filename: mammo raw file name.
% addChipBorder: (default is false) whether to add one pixel border when merging img from pics.

% number of chips
N = 72;
% size (rows and cols) of each chips
M = 288;

if nargin < 2
    addChipBorder = false;
end

%==========================================================================
% Read raw data into pics
%==========================================================================
pics = zeros(M, M, N);

[fid, errmsg] = fopen(filename, 'r', 'b');
if fid < 0
    disp(errmsg);
    return
end

for s = 1:N
    pics(:,:,s) = fread(fid, [M, M], 'int32');
end
fclose(fid);

% flip
pics = flip(pics, 1);

% change image vlue
% TODO: this is copied from function 'chipread'. I don't understand the meaning and purpose of this step.
pics = mod(floor(pics/(2^8)), 2^18);


%==========================================================================
% Tile pics into 2D img
%==========================================================================
% number of tiles for each row and col
tRows = 6;
tCols = 12;

% 2D image size and gap
if addChipBorder
    rows = tRows * M + tRows - 1;
    cols = tCols * M + tCols - 1;
    gap = M + 1;
else
    rows = tRows * M;
    cols = tCols * M;
    gap = M;
end

%---------------------------------------------------------
% upper left position of each tile/chip in img
%---------------------------------------------------------
pos = zeros(N, 2);

n = 1;
for tCol = tCols:-1:1
    for tRow = 1:tRows
        pos(n,1) = gap*(tRow - 1) + 1;
        pos(n,2) = gap*(tCol - 1) + 1;
        n = n + 1;
    end
end

%---------------------------------------------------------
% asign value to img
%---------------------------------------------------------
img = zeros(rows, cols);

n = 1;
for tCol = tCols:-1:1
    for tRow = 1:tRows
        img((0:M-1)+pos(n,1), (0:M-1)+pos(n,2)) = pics(:,:,n);
        n = n + 1;
    end
end


end