function y = MgInpaint1D(y, mask)
% Inpaint 1D array using linear interpolation/extrapolation method.
% y: 1xN or Nx1 array to be inpainted.
% mask: same size as y, values are ture or false indicating which element should be inpainted.

x = 1:numel(y);

y(mask) = interp1(x(~mask), y(~mask), x(mask), 'linear', 'extrap');


end