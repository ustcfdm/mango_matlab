function [p, mu1, mu2] = MgDePolyfit(x, y, z, order)
% Perform Dual-Energy polynomial fitting with given highest order.
% x: 1d array
% y: 1d array
% z: z = p1 + p2*x + p3*y + p4*x^2 + p5*x*y + p6*y^2 + ...
% orders: from 1 to 4.
% p: the coefficients corresponding to each order term (c1, c2 ...)
% mu1, mu2: normalization of x and y. i.e.
%           x = (x - mu1(1)) / mu1(2)
%           y = (y - mu2(1)) / mu2(2)

x = reshape(x, [], 1);
y = reshape(y, [], 1);
z = reshape(z, [], 1);

if nargout == 3
    mu1 = [mean(x), std(x)];
    mu2 = [mean(y), std(y)];

    x = (x - mu1(1)) / mu1(2);
    y = (y - mu2(1)) / mu2(2);
end


if order == 1
    A = cat(2, ones(size(x)), x, y);
elseif order == 2
    A = cat(2, ones(size(x)), x, y, x.^2, x.*y, y.^2);
elseif order == 3
    A = cat(2, ones(size(x)), x, y, x.^2, x.*y, y.^2, x.^3, x.^2.*y, x.*y.^2, y.^3);
elseif order == 4
    A = cat(2, ones(size(x)), x, y, x.^2, x.*y, y.^2, x.^3, x.^2.*y, x.*y.^2, y.^3, ...
        x.^4, x.^3.*y, x.^2.*y.^2, x.*y.^3, y.^4);
end

p = (A'*A) \ (A'*z);

end