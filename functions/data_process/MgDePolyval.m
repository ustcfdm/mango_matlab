function z = MgDePolyval(x, y, p, mu1, mu2)
% Perform Dual-Energy polynomial fitting with given highest order.
% x, y: input data.
% z: z = p1 + p2*x + p3*y + p4*x^2 + p5*x*y + p6*y^2 + ...
% p: the coefficients corresponding to each order term (c1, c2 ...)
% mu1, mu2: normalization of x and y. i.e.
%           x = (x - mu1(1)) / mu1(2)
%           y = (y - mu2(1)) / mu2(2)

if nargin == 5
    x = (x - mu1(1)) / mu1(2);
    y = (y - mu2(1)) / mu2(2);
end


switch numel(p)
    case 3
        z = p(1) + p(2)*x + p(3)*y;
    case 6
        z = p(1) + p(2)*x + p(3)*y + p(4)*x.^2 + p(5)*x.*y + p(6)*y.^2;
    case 10
        z = p(1) + p(2)*x + p(3)*y + p(4)*x.^2 + p(5)*x.*y + p(6)*y.^2 + ...
            p(7)*x.^3 + p(8)*x.^2.*y + p(9)*x.*y.^2 + p(10)*y.^3;
    case 15
        z = p(1) + p(2)*x + p(3)*y + p(4)*x.^2 + p(5)*x.*y + p(6)*y.^2 + ...
            p(7)*x.^3 + p(8)*x.^2.*y + p(9)*x.*y.^2 + p(10)*y.^3 + ...
            p(11)*x.^4 + p(12)*x.^3.*y + p(13)*x.^2.*y.^2 + p(14)*x.*y.^3 + p(15)*y.^4;
    otherwise
        error('Invaid parameter "p"!\n');
end

end

